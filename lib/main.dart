import 'package:flutter/material.dart';

void main() => runApp(BelajarImage());

class BelajarImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(
        title: Text("Menampilkan gambar di flutter"),
      ),
      body: ClipOval(
        child: Image(width: 300, height: 300, image: AssetImage('assets/images/gambar.jpg'), fit: BoxFit.cover),
      ),
    ));
  }
}
